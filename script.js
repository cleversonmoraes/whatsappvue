// Variavel simulando um banco de dados
var users = [
    {
        "active": false,
        "name": "Joao José Campos",
        "contact": 22222222222,
        "photo": "./img/Avatar.png"
    }, {
        "active": false,
        "name": "Elaine Da Silva",
        "contact": 33333333333,
        "photo": "./img/Avatar (1).png"
    }, {
        "active": false,
        "name": "Camila Aparecida",
        "contact": 44444444444,
        "photo": "./img/Avatar (2).png"
    }, {
        "active": false,
        "name": "Jose Batista Souza",
        "contact": 55555555555,
        "photo": "./img/Avatar (3).png"
    }, {
        "active": false,
        "name": "Léia de Souza Bueno",
        "contact": 66666666666,
        "photo": "./img/Avatar (4).png"
    }, {
        "active": false,
        "name": "Júlio Santos",
        "contact": 77777777777,
        "photo": "./img/Avatar (5).png"
    }, {
        "active": false,
        "name": "Julio Cesar Lopes",
        "contact": 88888888888,
        "photo": "./img/Avatar (6).png"
    }, {
        "active": false,
        "name": "Carlos Tozeli",
        "contact": 99999999999,
        "photo": "./img/Avatar (7).png"
    }, {
        "active": false,
        "name": "Grasielle Castro",
        "contact": 10101010101,
        "photo": "./img/Avatar (8).png"
    },
    {
        "active": false,
        "name": "Ítala Farias",
        "contact": 11111111111,
        "photo": "./img/Avatar (9).png"
    },
]

const CanalComunicacao = {
    data() {
        return {
            users: window.users,
            usuariosSelecionados: [],
            talkStart: false
        }
    },
    methods: {
        // Seleciona usuarios para iniciar conversa
        selecionar(event, user) {
            let talk = document.querySelector('.talk')
            let call = document.querySelector('.call')


            talk.innerHTML = ''
            call.innerHTML = ''

            this.usuariosSelecionados = [];


            let name = (user.name)
            let photo = (user.photo)
            let phoneNumber = (user.contact)


            // Armazena dados do usuario selecionado em um array para uso posterior
            this.usuariosSelecionados.push({ name, photo, phoneNumber });
            this.startCall()
            user.active = !user.active

            if (!user.active) {
                this.finalizar()
            }


        },
        // Finaliza a conversa com o usuário
        finalizar() {
            this.usuariosSelecionados = [];
            let talk = document.querySelector('.talk')
            let call = document.querySelector('.call')
            let dayCall = document.querySelector('.day')

            talk.innerHTML = ''
            call.innerHTML = ''
            dayCall.innerHTML = ''
            this.talkStart = false
        },
        // Configurações para conversar com o usuário
        enviar(event) {
            event.preventDefault()
            let text = document.querySelector('.resposta').value

            if (text !== '') {
                if (this.usuariosSelecionados.length > 0) {

                    let talk = document.querySelector('.talk')
                    let newParagraph = document.createElement('p')
                    let response = document.querySelector('.resposta')


                    newParagraph.textContent = text

                    talk.appendChild(newParagraph)
                    response.value = ''
                    this.talkStart = true
                } else {
                    alert('Por favor, selecione um usuário para enviar sua mensagem!')
                }
            }
        },
        // Inicia a conversa, cria protocolo e data
        startCall() {
            let call = document.querySelector('.call');
            let protocolo = Math.floor(Math.random() * 10000000);

            call.innerHTML = 'Protocolo: ' + protocolo;

            let dayCall = document.querySelector('.day')

            let monthNames = [
                "Janeiro", "Fevereiro", "Março",
                "Abril", "Maio", "Junho",
                "Julho", "Agosto", "Setembro",
                "Outubro", "Novembro", "Dezembro"
            ];
            let day = new Date().getDate()
            let mes = new Date().getMonth()
            let hour = new Date().getHours()
            let minutes = new Date().getMinutes()

            dayCall.innerHTML = day + ' ' + monthNames[mes]
        }
    }
}
Vue.createApp(CanalComunicacao).mount('#app')